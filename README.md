#Nota importante

##LARAVEL COPIAR ESTE CODIGO EN route/api.php

```php
Route::post('/android', function(Request $request) {
	$pass = Hash::make($request->password);
	$user = User::where('email', $request->email)->first();
	if (!$user ) {

		return response()
		->json(['status' => 'Sus datos no coinciden']);
	}

	if ($user ){

		$user->setAttribute('status', 'success');
		if (!Hash::check($request->password, $user->password)) {
			return response()
			->json(['status' => 'Clave invalida!']);
		}

		return $user;
	}
});

```