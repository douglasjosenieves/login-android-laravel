package com.example.dnieves.weblink;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import android.view.View;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

//import static com.example.dnieves.weblink.LoginActivity.mLoginFormView;
//import static com.example.dnieves.weblink.LoginActivity.mProgressView;


/**
 * Created by ProgrammingKnowledge on 1/5/2016.
 */
public class BackgroundWorker extends AsyncTask<String, Void, String> {
    Context context;
    AlertDialog alertDialog;


    BackgroundWorker(Context ctx) {
        context = ctx;

    }

    @Override
    protected String doInBackground(String... params) {
        //showProgress(true);
        String type = params[0];
        String login_url = "http://8f9f2819.ngrok.io/api/android";
        if (type.equals("login")) {
            try {
                String user_name = params[1];
                String password = params[2];
                URL url = new URL(login_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
                String post_data = URLEncoder.encode("email", "UTF-8") + "=" + URLEncoder.encode(user_name, "UTF-8") + "&"
                        + URLEncoder.encode("password", "UTF-8") + "=" + URLEncoder.encode(password, "UTF-8");
                bufferedWriter.write(post_data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
                String result = "";
                String line = "";
                while ((line = bufferedReader.readLine()) != null) {
                    result += line;
                }
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return result;
            } catch (MalformedURLException e) {
                e.printStackTrace();
                cancel(true);
            } catch (IOException e) {
                e.printStackTrace();
                cancel(true);
            }
        }
        return null;
    }

    @Override
    protected void onPreExecute() {
        alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle("Login Status");
    }

    @Override
    protected void onPostExecute(String result) {
//        mProgressView.setVisibility(View.GONE);
        try {
            JSONObject jsonObject = new JSONObject(result);
            String status = jsonObject.get("status").toString();

            if (status.equals("success")) {
                String name = jsonObject.get("name").toString();
                alertDialog.setTitle("Login Success");
                alertDialog.setMessage("Welcome " + name);
                alertDialog.show();

            } else {
                alertDialog.setMessage(status);
                alertDialog.show();

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onProgressUpdate(Void... values) {
//        mProgressView.setVisibility(View.GONE);
//        mLoginFormView.setVisibility(View.VISIBLE);

        super.onProgressUpdate(values);
    }

    @SuppressLint("WrongConstant")
    @Override
    protected void onCancelled() {
//        mProgressView.setVisibility(View.GONE);
//        mLoginFormView.setVisibility(View.VISIBLE);

        super.onCancelled();

        alertDialog.setTitle("Disculpe!");
        alertDialog.setMessage("Problema de conexion intente mas tarde!");
        alertDialog.show();


    }


}